## EMP Shield Discount

**Save $50 OFF** when you buy EMP Shield Here:

[EMP Shield Discount](https://empshield.discount).

EMP Shield offers $25k in protection against disasters.

Tested and certified by the Department of Homeland Security.

**Use code 'Discount' at checkout** or use the link to save!